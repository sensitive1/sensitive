package controllers

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"

	"server/helpers"
	"server/models"
	"server/services"
)

func InitHomeController() *HomeController {

	homeService := new(services.HomeService)

	homeController := new(HomeController)
	homeController.HomeService = homeService

	rand.Seed(time.Now().Unix())
	return homeController
}

type HomeController struct {
	HomeService services.IHomeService
}

func (p *HomeController) Analyze(res http.ResponseWriter, req *http.Request) {

	var dataBody models.Text

	err := json.NewDecoder(req.Body).Decode(&dataBody)

	if err != nil {
		helpers.ResponseBadRequest(res, http.StatusBadRequest, err)
		return
	}

	result, err := p.HomeService.Analyze(dataBody)
	if err == nil {
		helpers.Response(res, http.StatusOK, result)
	} else {
		helpers.ResponseBadRequest(res, http.StatusBadRequest, err)
	}

	return
}
