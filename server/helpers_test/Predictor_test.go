package helpers_test

import (
	"math/rand"
	"server/helpers"
	"strconv"
	"testing"
)

func TestAccuracy(t *testing.T) {

	maxTestlabel := 50

	testCsv := helpers.ReadCSV("../train_df_full.csv")

	testDataPerLabel := make(map[string][]string)
	trainData := make([][]string, 0)

	totalData := len(testCsv)

	for i := 0; i < totalData; i++ {

		testDataPerLabel[testCsv[i][1]] = append(testDataPerLabel[testCsv[i][1]], testCsv[i][0])
	}

	// separate train and test. labels for test is at most 50
	for label, tweets := range testDataPerLabel {
		rand.Seed(29)

		//fmt.Println("============= UKURAN ======== ", label, len(tweets))

		rand.Shuffle(len(tweets), func(i, j int) { tweets[i], tweets[j] = tweets[j], tweets[i] })

		for i := 0; i < len(tweets)-maxTestlabel; i++ {
			theTweet := tweets[i]
			dataPoint := []string{theTweet, label}

			trainData = append(trainData, dataPoint)

		}
		testDataPerLabel[label] = testDataPerLabel[label][len(tweets)-maxTestlabel:]

		if len(testDataPerLabel[label]) != maxTestlabel {
			t.Errorf("Number labels in %s validation is %d, not %d", label, len(testDataPerLabel[label]), maxTestlabel)
		}
	}

	p := helpers.Predictor{}
	p.Init(10)

	p.Fit(trainData, 4)

	// row: predicted label, col: actual label
	confusionMatrix := make(map[string]map[string]int)
	labels := p.GetSentimentValues()

	for _, label := range labels {
		confusionMatrix[label] = make(map[string]int)
	}

	totalCorrect := 0
	totalLabels := 0
	for label, tweets := range testDataPerLabel {

		for _, tweet := range tweets {
			guessedLabel, _ := p.Predict(tweet)

			if guessedLabel == label {
				totalCorrect++
			}
			totalLabels++

			confusionMatrix[guessedLabel][label]++
		}

	}

	t.Logf("Confusion matrix:\n")
	t.Log("entries order:", labels)
	for _, label := range labels {

		buffer := ""
		for _, label2 := range labels {
			buffer += strconv.Itoa(confusionMatrix[label][label2]) + " "
		}
		t.Logf("%s\n", buffer)
	}
	t.Log()

	for _, label := range labels {

		truePositive := confusionMatrix[label][label]

		totalPositive := 0
		totalNegative := 0
		for _, label2 := range labels {
			totalPositive += confusionMatrix[label][label2]
			totalNegative += confusionMatrix[label2][label]
		}

		precision := float64(truePositive) / float64(totalPositive)
		recall := float64(truePositive) / float64(totalNegative)
		f1 := 2.0 * (precision * recall) / (precision + recall)

		t.Logf("Performance for tweet with sentiment %s:\n", label)
		t.Logf("Guessed %d tweets in this label correctly (out of %d)\n", truePositive, maxTestlabel)
		t.Logf("Precision: %f\n", precision)
		t.Logf("Recall: %f\n", recall)
		t.Logf("F1 score: %f\n\n", f1)
	}

	t.Logf("Guessed %d tweets correctly in total (Precision: %f\n)", totalCorrect, float64(totalCorrect)/float64(totalLabels))
}
