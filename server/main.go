package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	pq "github.com/lib/pq"
	"server/controllers"
	"server/helpers"
)

func registerPing(db *sql.DB) {

	_, err := db.Exec("INSERT INTO ping_timestamp (occurred) VALUES ($1)", time.Now())
	if err != nil {
		log.Println("Couldn't insert the ping")
		log.Println(err)
	}
}

func pingFunc(res http.ResponseWriter, req *http.Request) {
	DBURL := os.Getenv("DATABASE_URL")
	log.Printf("DB [%s]", DBURL)
	db, err := sql.Open("postgres", DBURL)
	if err != nil {
		log.Fatalf("Error opening database: %q", err)
	}
	log.Println("booyah")

	defer registerPing(db)
	r := db.QueryRow("SELECT occurred FROM ping_timestamp ORDER BY id DESC LIMIT 1")
	var lastDate pq.NullTime
	r.Scan(&lastDate)

	message := "first time!"
	if lastDate.Valid {
		message = fmt.Sprintf("%v ago", time.Now().Sub(lastDate.Time).String())
	}

	helpers.Response(res, http.StatusOK, message)

}

func main() {
	
	r := mux.NewRouter().StrictSlash(false)
	// Serving static content from web - we will populate this from within the docker container
	//r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("./web"))))

	api := r.PathPrefix("/api").Subrouter()

	api.HandleFunc("/ping", pingFunc).Methods("GET")

	homeController := controllers.InitHomeController()

	api.HandleFunc("/analyze", homeController.Analyze).Methods("POST")

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./web")))

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		fmt.Printf("caught sig: %+v", sig)
		fmt.Println("Wait for 2 second to finish processing")
		time.Sleep(2 * time.Second)
		os.Exit(0)
	}()

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "8080"
	}
	fmt.Println("Server served at port " + PORT)
	if err := http.ListenAndServe(":"+PORT, r); err != nil {
		log.Fatal("Unable to start service: " + err.Error())
	}

}
