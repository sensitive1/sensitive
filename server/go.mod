module server

go 1.13

require (
	github.com/gin-gonic/contrib v0.0.0-20201005132743-ca038bbf2944 // indirect
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/google/uuid v1.1.1 //
	github.com/gorilla/mux v1.7.3
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/lib/pq v1.1.1	
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
