package helpers

import "fmt"

// GenericError Struct for error handling
type GenericError struct {
	HTTPCode int
	Code     int
	Message  string
	Cause    error
}

func (e *GenericError) Error() string {
	errString := fmt.Sprintf("%+v", e.Cause)
	return errString
}
