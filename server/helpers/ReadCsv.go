package helpers

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func downloadFile(URL, fileName string) error {
	//Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		fmt.Println(response.StatusCode)
		return errors.New("Received non 200 response code")
	}
	//Create a empty file
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	//Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	fmt.Println(response.Body)
	if err != nil {
		return err
	}

	return nil
}

func ReadCSV(csvPath string) [][]string {

	_, err := os.Stat(csvPath)
	if os.IsNotExist(err) {
		fileName := csvPath
		URL := "https://files.catbox.moe/bjvssx.csv"
		err := downloadFile(URL, fileName)
		if err != nil {
			log.Fatal(err)
		}
	}
	var arr [][]string
	csvFile, err := os.Open(csvPath)
	if err != nil {
		panic(err.Error())
	}
	defer csvFile.Close()
	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		panic(err.Error())
	}
	for i, line := range csvLines {
		if i == 0 {
			continue
		}
		var sentence []string
		sentence = append(sentence, line[0])
		sentence = append(sentence, line[1])
		arr = append(arr, sentence)
	}
	return arr
}
