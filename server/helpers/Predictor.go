package helpers

import (
	"fmt"
	"math"
	"regexp"
	"sort"
	"strings"
)

type Predictor struct {
	//WeightData. All probabilities are in log
	sentimentProbability          map[string]float64
	sentenceGivenLabelProbability map[string]map[string]float64 //first: label, second: sentence
	sentenceProbability           map[string]float64
	sentenceLimit                 int

	//Label
	sentimentValues []string
}

func (p *Predictor) Init(sentenceLimit int) {

	p.sentenceLimit = sentenceLimit

	var sentimentArr []string
	sentimentArr = append(sentimentArr, "Pro Social Distancing", "Anti Social Distancing", "Provocation", "Neutral")
	p.sentimentValues = sentimentArr
}

func (p Predictor) existInLibrary(sentence string) bool {
	_, ok := p.sentenceProbability[sentence]
	return ok
}

func isInList(list []string, needle string) bool {
	for _, value := range list {
		if value == needle {
			return true
		}
	}

	return false
}

func Min(x int, y int) int {
	if x < y {
		return x
	}
	return y
}

func (p *Predictor) Fit(trainingData [][]string, minSentenceFrequency int) {

	sentimentValues := p.sentimentValues

	sentimentCount := make(map[string]int)                       //Total kalimat di suatu label
	sentencePerSentimentCount := make(map[string]map[string]int) //Frekuensi per kata di suatu label. First: label, second: sentence
	sentenceCount := make(map[string]int)

	for _, sentiment := range sentimentValues {
		sentencePerSentimentCount[sentiment] = make(map[string]int)
	}

	totalTweets := len(trainingData)

	for i := 0; i < totalTweets; i++ {

		sentimentValue := trainingData[i][1]

		if !isInList(sentimentValues, sentimentValue) {
			fmt.Println("[ERROR]", trainingData[i][0], trainingData[i][1], "Has unsupported label")
			return
		}

		words := strings.Split(trainingData[i][0], " ")

		occuredSentence := make(map[string]bool)

		for l := 0; l < len(words); l++ {
			limit := Min(l+p.sentenceLimit, len(words))
			for r := l; r < limit; r++ {
				sentence := strings.Join(words[l:r+1], " ")

				occuredSentence[sentence] = true

			}
		}

		for sentence, _ := range occuredSentence {
			sentencePerSentimentCount[sentimentValue][sentence]++
			sentenceCount[sentence]++
		}

		sentimentCount[sentimentValue]++
	}

	// erase low occuring sentences from corvus
	for sentence, freq := range sentenceCount {
		if freq < minSentenceFrequency {
			delete(sentenceCount, sentence)
			for sentiment := range sentencePerSentimentCount {
				delete(sentencePerSentimentCount[sentiment], sentence)
			}
		}
	}

	//Count message probability for each sentiment
	sentimentProbability := make(map[string]float64)
	sentenceGivenLabelProbability := make(map[string]map[string]float64)
	sentenceProbability := make(map[string]float64)

	for sentiment, freq := range sentimentCount {
		sentimentProbability[sentiment] = math.Log(float64(freq)) - math.Log(float64(totalTweets))
		//sentimentProbability[sentiment] = 0.25
	}

	for sentence, freq := range sentenceCount {
		sentenceProbability[sentence] = math.Log(float64(freq)) - math.Log(float64(totalTweets))
	}

	differentSentencesCount := len(sentenceCount)
	//fmt.Println("different sentences: ", differentSentencesCount)

	//sentence probability
	for sentiment, sentenceMap := range sentencePerSentimentCount {

		sentenceGivenLabelProbability[sentiment] = make(map[string]float64)

		for sentence, freq := range sentenceMap {
			normFreq := float64(freq) * float64(totalTweets) / float64(sentimentCount[sentiment])
			sentenceGivenLabelProbability[sentiment][sentence] = math.Log(float64(normFreq+1)) - math.Log(float64(sentimentCount[sentiment]+differentSentencesCount))
		}

		sentenceGivenLabelProbability[sentiment]["unknown"] = 0 - math.Log(float64(sentimentCount[sentiment]+differentSentencesCount))
	}

	p.sentimentProbability = sentimentProbability
	p.sentenceGivenLabelProbability = sentenceGivenLabelProbability
	p.sentenceProbability = sentenceProbability

}

//Calculation for combined probability (prediction)
func (p *Predictor) calcCombinedProba(input string) (map[string]float64, map[string]map[string]float64) {

	tweetGivenSentenceProbability := make(map[string]map[string]float64) // probability the tweet label is "first" given the sentence "second"
	tweetLabelProbability := make(map[string]float64)                    // probabiity that the tweet is a given label

	words := strings.Split(input, " ")

	//Calculate combined probability
	for _, sentiment := range p.sentimentValues {

		tweetGivenSentenceProbability[sentiment] = make(map[string]float64)
		hasKnownWord := false
		usedWord := make(map[string]bool)
		var curProba float64 = p.sentimentProbability[sentiment]

		misCount := 0

		for l := 0; l < len(words); l++ {
			limit := Min(l+p.sentenceLimit, len(words))
			for r := l; r < limit; r++ {
				currentSentence := strings.Join(words[l:r+1], " ")

				if !usedWord[currentSentence] && p.existInLibrary(currentSentence) {
					//fmt.Println("yes")
					if !hasKnownWord {
						hasKnownWord = true
					}
					if _, ok := p.sentenceGivenLabelProbability[sentiment][currentSentence]; !ok {
						currentSentence = "unknown"
						misCount++
					}

					usedWord[currentSentence] = true
					sentenceSignificance := p.sentenceGivenLabelProbability[sentiment][currentSentence]

					if currentSentence != "unknown" {
						tweetGivenSentenceProbability[sentiment][currentSentence] = p.sentimentProbability[sentiment] + sentenceSignificance - p.sentenceProbability[currentSentence]
					}

					curProba += sentenceSignificance
				}
			}
		}
		//fmt.Println(tweetGivenSentenceProbability)
		if hasKnownWord {
			tweetLabelProbability[sentiment] = curProba
		}
	}

	return tweetLabelProbability, tweetGivenSentenceProbability

}

// helper class for storing ranking of words
type WordRank struct {
	Word  string
	Proba float64
}

//Rank dominant word
func (p *Predictor) rankWord(sentimentGivenSentenceProbability map[string]map[string]float64, sentiment string) []WordRank {
	if sentiment != "unindentified" {

		wordRanking := make([]WordRank, len(sentimentGivenSentenceProbability[sentiment]))
		id := 0
		for sentence, proba := range sentimentGivenSentenceProbability[sentiment] {
			var wr WordRank
			wr.Word = sentence
			wr.Proba = proba
			wordRanking[id] = wr
			id++
		}

		// sort by largest proba
		sort.SliceStable(wordRanking, func(i, j int) bool {
			if wordRanking[i].Proba != wordRanking[j].Proba {
				return wordRanking[i].Proba > wordRanking[j].Proba
			} else {
				return wordRanking[i].Word < wordRanking[j].Word
			}
		})

		// trimmedSize := 3
		// if len(wordRanking) < 3 {
		// 	trimmedSize = len(wordRanking)
		// }

		//wordRanking = wordRanking[:trimmedSize]

		// removing probas lower than 0.05
		for len(wordRanking) > 1 && math.Exp(wordRanking[len(wordRanking)-1].Proba) < 0.05 {
			wordRanking = wordRanking[:len(wordRanking)-1]
		}

		//fmt.Println(wordRanking)
		return wordRanking

	} else {
		panic("There's no dominant word in this sentence right now")
	}
}

//Preprocess the string first
func PreprocessString(sentence string) string {
	reg, _ := regexp.Compile("[^a-zA-Z# ]+")
	res := strings.ToLower(reg.ReplaceAllString(sentence, ""))

	// remove extra space
	extraSpaceParser := regexp.MustCompile(`\s+`)
	res = extraSpaceParser.ReplaceAllString(res, " ")

	return res
}

func (p *Predictor) Predict(input string) (string, []WordRank) {

	input = PreprocessString((input))

	ansString := "Unknown"

	tweetLabelProbability, tweetGivenSentenceProbability := p.calcCombinedProba(input)
	for sentiment, proba := range tweetLabelProbability {
		//fmt.Println(sentiment, tweetLabelProbability[sentiment])
		if ansString == "Unknown" || proba > tweetLabelProbability[ansString] {
			//fmt.Println("Hiya", ansString, sentiment)
			ansString = sentiment
		}
	}

	// special case: pro and anti has similar proba. predict neutral instead
	if ansString == "Pro Social Distancing" || ansString == "Anti Social Distancing" {
		if math.Abs(tweetLabelProbability["Pro Social Distancing"]-tweetLabelProbability["Anti Social Distancing"]) < 0.00001 {
			ansString = "Neutral"
		}
	}
	//fmt.Println()
	wordRanking := p.rankWord(tweetGivenSentenceProbability, ansString)
	return ansString, wordRanking
}

func (p *Predictor) GetSentimentValues() []string {
	return p.sentimentValues
}
