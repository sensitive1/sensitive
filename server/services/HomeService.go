package services

import (
	"errors"
	"server/helpers"
	"server/models"
	"strings"
)

var predictor helpers.Predictor

func init() {
	predictor = helpers.Predictor{}
	predictor.Init(10)

	csvData := helpers.ReadCSV("train_df_full.csv")
	predictor.Fit(csvData, 4)
}

type IHomeService interface {
	Analyze(models.Text) (models.TextAnalyzed, error)
}

type HomeService struct {
}

func foundElement(target string, words []string) bool {
	for i := 0; i < len(words); i++ {
		if words[i] == target {
			return true
		}
	}
	return false
}

// check if exist sequence as subsequence of paragraph from index start. Returns the end of sequence and T/F wheter found
func containsSubsequence(sequence []string, paragraph []string, start int) (int, bool) {

	if helpers.PreprocessString(paragraph[start]) != sequence[0] {
		return -1, false
	}

	curSeqIndex := 0
	for index := start; index < len(paragraph); index++ {

		curParaWord := paragraph[index]
		curParaWord = helpers.PreprocessString(curParaWord)

		if curParaWord == sequence[curSeqIndex] {
			curSeqIndex++
		}

		if curSeqIndex == len(sequence) {
			return index, true
		}

	}

	return -1, false
}

func (s HomeService) AnalyzeHighlight(text string, importantWords []string) (res string) {
	textSplit := strings.Split(text, " ")

	// positions where to put marks. markedStart: opening, markedEnd: closing
	markedStart := make([]int, len(textSplit))
	markedEnd := make([]int, len(textSplit))

	for _, importantWord := range importantWords {

		importantTokenized := strings.Split(importantWord, " ")

		for i := range textSplit {
			endInd, ok := containsSubsequence(importantTokenized, textSplit, i)

			if ok {
				markedStart[i]++
				markedEnd[endInd]++
			}
		}
	}

	// stack algorithm to add marks. Will merge two overlap segments
	accumulator := 0
	for i := range textSplit {
		if markedStart[i] > 0 && accumulator == 0 {
			textSplit[i] = "<mark>" + textSplit[i]
		}
		accumulator += markedStart[i]

		if markedEnd[i] > 0 && accumulator == markedEnd[i] {
			textSplit[i] = textSplit[i] + "</mark>"
		}
		accumulator -= markedEnd[i]

	}

	return strings.Join(textSplit, " ")
}

func (s HomeService) Analyze(reqBody models.Text) (res models.TextAnalyzed, err error) {

	text := reqBody.Text

	// checker so that we are not hit with large bruteforce
	if len(text) > 2000 {
		err = errors.New("Text too long, maximum is 2000 characters")
		return
	}

	res.Text = text
	label, wordRanking := predictor.Predict(text)
	res.Label = label

	// highlight the best part of the text
	importantWords := make([]string, len(wordRanking))
	for i, _ := range wordRanking {
		importantWords[i] = wordRanking[i].Word
	}

	var textHighlighted string
	if label != "Unknown" {
		textHighlighted = s.AnalyzeHighlight(text, importantWords)
	} else {
		textHighlighted = "Cannot determine sentiment of the text. the Text may be too short, lack context, or not written in English."
	}

	res.TextAnalyzed = textHighlighted
	return
}
