package models

type Text struct {
	Text string `json:"text"`
}

type TextAnalyzed struct {
	Text         string `json:"text"`
	Label        string `json:"label"`
	TextAnalyzed string `json:"analyzedText"`
}
