import React, { Component } from 'react';
import './App.css';
import Logo from "./sensitive_logo.png";
import axios from 'axios';
import {Button, Container, Form, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Loader from 'react-loader-spinner';

import { ThemeProvider } from 'styled-components';
import { GlobalStyles, lightTheme, darkTheme } from './global';

class App extends Component{
  constructor(props) {
    super(props);
    this.state = {
      text: "", 
      label: "placeholder", 
      analyzedText: "placeholder",
      loading: false,
      initialized: false,
      chars_left: 2000,
      mode: "dark"
    }
    this.handleChange = this.handleChange.bind(this);
    this.analyze = this.analyze.bind(this);
    this.getFontTheme = this.getFontTheme.bind(this);
    this.getButtonTheme = this.getButtonTheme.bind(this);
  } 
  componentDidMount() {
    this.setState({initialized: false});
  }
  handleChange(event) {
    this.setState({text: event.target.value, chars_left:2000-event.target.value.length});
  }
  timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  getFontTheme(){
    return {
      color: this.state.mode === "dark" ? "black" : "white"
    }
  }
  getButtonTheme(){
    return {
      background:  this.state.mode === "dark" ? "#555555" : "#8F9DA5"
    }
  }
  async analyze(){
    if (!this.state.initialized)
    this.setState({initialized: true});
    this.setState({loading: true});
    await axios.post('api/analyze', {text: this.state.text}, {timeout: 5000})
        .then((response) => {
            this.setState(() => {
              var res = response.data.data;
              //await delay(5000);
              return { 
                text: res.text, 
                label: res.label, 
                analyzedText: res.analyzedText
              }
            })
        })
        .catch(function (error) {
          alert(error.response.data.error);
          return;
        })
        .then(() => this.timeout(2000))
        .then(() => this.setState({loading:false}));
        
  }
  
  async changeMode()  {
    this.setState({mode:(this.state.mode==="dark" ? "light" : "dark")})
  }

  render(){
    return (
      <ThemeProvider theme={this.state.mode==="light" ? lightTheme : darkTheme}>
          <Container>
            <GlobalStyles />
            <div>
              <Button variant={this.state.mode === "light" ? "dark" : "light"} style={{position: "absolute", top: 0, right: 0}} onClick={() => {this.changeMode()}}>{this.state.mode === "light" ? "Dark Mode" : "Light Mode"}</Button>
            </div>
            <img class="rounded mx-auto d-block mt-3" alt="sensitive" src={Logo}  />
            <h1 class="text-center" id="title">SensiTive</h1>
            <h2 class="text-center" id="subtitle">Covid Sentimental Analysis Tool</h2>
            <div class="d-flex justify-content-center">
              <Form.Group controlId="exampleForm.ControlTextarea1" class="text-center">
                <Row>
                  <Col>
                    <Form.Control as="textarea" cols="10" rows="5" maxlength="2000" value={this.state.text} onChange={this.handleChange} id="text-form" placeholder="Insert sentence(s) here..."/>
                    <p>Characters remaining: {this.state.chars_left}</p>
                  </Col>
                </Row>
                <Row>
                  <Button style ={this.getButtonTheme()}   onClick={() => {this.analyze(this.state.text)}} id="submitButton">Analyze!</Button>
                </Row>
              </Form.Group>
            </div>
            {this.state.initialized && <div class="output">
              {this.state.loading ?  
              <div style={{width: "100%", height: "100",display: "flex",justifyContent: "center",alignItems: "center"}}>
                  <Loader type="ThreeDots" color="#8F9DA5"  />
              </div>
              :  
              <div class="result">
                <div class="resultSentiment">
                  <Row className="d-flex justify-content-md-around flex-sm-row">
                    <Col md="auto">
                        <div class="header1">
                          <h3  style = {this.getFontTheme()} class="text-center">Sentence Sentiment</h3>
                        </div>
                        <div   class="Content">
                          <h5 class="text-center">{this.state.label}</h5>
                        </div>
                    </Col>
                  </Row>
                </div>
                <div class="resultHighlight">
                  <div class="header2">
                    <h3 style = {this.getFontTheme()} class="text-center">Strong Indicators Of Statement</h3>
                  </div>
                  <div class="Content2">
                    <div dangerouslySetInnerHTML={{ __html: this.state.analyzedText}} class="Content2Text"></div>
                  </div>
                </div>
              </div>
            }
          </div>}
          </Container>
        </ThemeProvider>
      );
    }
  }


export default App;
