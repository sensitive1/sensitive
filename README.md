# Sensitive: Sentimental Analytics of COVID Related Text

Web url: <sensitive.herokuapp.com>

This is a sentiment analysis created to analyze if a text is either Pro Social Distancing, Anti Social Distancing, Provocation, or Neutral created using Naive Bayes Algorithm. We took a different approach, instead of converting the text to a vector, we directly process the text, by taking every possible contiguous sentence of maximum length 10.

The model is trained on 2000 COVID tweets from the US and UK, obtained from here: https://www.kaggle.com/smid80/coronavirus-covid19-tweets

The F1 Score of this classifier is 75%.

